import { all, call, put, take, takeEvery, } from "redux-saga/effects";
import { eventChannel } from "redux-saga";
import { Record } from "immutable";
import { appName } from "../config";
import apiService from "../services/api";
import { createSelector } from "reselect"

/**
 * Constants
 * */
export const moduleName = "texts";
const prefix = `${appName}/${moduleName}`;

export const FETCH_TEXTS_REQUEST = `${prefix}/FETCH_TEXTS_REQUEST`;
export const FETCH_TEXTS_SUCCESS = `${prefix}/FETCH_TEXTS_SUCCESS`;
export const FETCH_TEXTS_ERROR = `${prefix}/FETCH_TEXTS_ERROR`;
export const ADD_TEXTS_REQUEST = `${prefix}/ADD_TEXTS_REQUEST`;

/**
 * Reducer
 * */
export const ReducerRecord = Record({
  texts: null,
  loading: false,
  error: null,
});

export default function reducer(state = new ReducerRecord(), action) {
  const { type, payload, error } = action;

  switch (type) {
    case FETCH_TEXTS_REQUEST:
      return state.set("loading", true);

    case FETCH_TEXTS_SUCCESS:
      return state
        .set("loading", false)
        .set("texts", payload.texts)
        .set("error", null);

    default:
      return state;
  }
}

/**
 * Selectors
 * */

 export const textsSelector = (state) => state[moduleName].texts


export const getAboutText = createSelector([textsSelector], (texts) => {
  if (texts) {
    return texts.about
  }
})

export const getArgeementText = createSelector([textsSelector], (texts) => {
  if (texts) {
    return texts.agreement
  }
})

/**
 * Custom Hooks
 */


/**
 * Action Creators
 * */

export const fetchTextsFromServer = () => ({
  type: FETCH_TEXTS_REQUEST,
});

export const addTexts = (texts) => {
  return {
    type: ADD_TEXTS_REQUEST,
    payload: { ...texts }
  }
};


/**
 * Sagas
 */

export const createTextsChanel = () => eventChannel(emit => apiService.fetchTexts(emit));

export const syncTextsState = function* () {
  const chanel = yield call(createTextsChanel);

  while (true) {
    const { texts } = yield take(chanel);
    if (texts) {
      yield put({
        type: FETCH_TEXTS_SUCCESS,
        payload: { texts },
      });
    } else {
      yield put({
        type: FETCH_TEXTS_ERROR,
      });
    }
  }
};

export const addTextsSaga = function* ({ payload }) {
  yield call(apiService.addTexts, payload);
}

export const saga = function* () {
  yield all([
    takeEvery(FETCH_TEXTS_REQUEST, syncTextsState),
    takeEvery(ADD_TEXTS_REQUEST, addTextsSaga),
  ]);
};
