import { all, call, put, take, takeEvery, } from "redux-saga/effects";
import { eventChannel } from "redux-saga";
import { Record } from "immutable";
import { appName } from "../config";
import apiService from "../services/api";
import { createSelector } from "reselect"

/**
 * Constants
 * */
export const moduleName = "achievements";
const prefix = `${appName}/${moduleName}`;

export const FETCH_ACHIEVS_REQUEST = `${prefix}/FETCH_ACHIEVS_REQUEST`;
export const FETCH_ACHIEVS_SUCCESS = `${prefix}/FETCH_ACHIEVS_SUCCESS`;
export const FETCH_ACHIEVS_ERROR = `${prefix}/FETCH_ACHIEVS_ERROR`;
export const ADD_ACHIEVS_REQUEST = `${prefix}/ADD_ACHIEVS_REQUEST`;

/**
 * Reducer
 * */
export const ReducerRecord = Record({
  achievements: null,
  loading: false,
  error: null,
});

export default function reducer(state = new ReducerRecord(), action) {
  const { type, payload, error } = action;

  switch (type) {
    case FETCH_ACHIEVS_REQUEST:
      return state.set("loading", true);

    case FETCH_ACHIEVS_SUCCESS:
      return state
        .set("loading", false)
        .set("achievements", payload.achievements)
        .set("error", null);

    default:
      return state;
  }
}

/**
 * Selectors
 * */

/**
 * Custom Hooks
 */


/**
 * Action Creators
 * */

export const fetchAchievsFromServer = () => ({
  type: FETCH_ACHIEVS_REQUEST,
});

export const addAchievements = (achievements) => {
  return {
    type: ADD_ACHIEVS_REQUEST,
    payload: { ...achievements }
  }
};


/**
 * Sagas
 */

export const createAchievsChanel = () => eventChannel(emit => apiService.fetchAchievs(emit));

export const syncAchievsState = function* () {
  const chanel = yield call(createAchievsChanel);

  while (true) {
    const { achievements } = yield take(chanel);
    if (achievements) {
      yield put({
        type: FETCH_ACHIEVS_SUCCESS,
        payload: { achievements },
      });
    } else {
      yield put({
        type: FETCH_ACHIEVS_ERROR,
      });
    }
  }
};

export const addAchievsSaga = function* ({ payload }) {
  yield call(apiService.addAchievs, payload);
}

export const saga = function* () {
  yield all([
    takeEvery(FETCH_ACHIEVS_REQUEST, syncAchievsState),
    takeEvery(ADD_ACHIEVS_REQUEST, addAchievsSaga),
  ]);
};
