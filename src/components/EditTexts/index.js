import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { Button } from '@material-ui/core';
import { useDispatch, useSelector } from "react-redux";
import { getAboutText, getArgeementText, fetchTextsFromServer, addTexts } from '../../ducks/texts'
import TextField from '@material-ui/core/TextField';

const EditTexts = () => {
  const dispatch = useDispatch();
  const aboutTextDefault = useSelector(getAboutText)
  const agreementTextDefault = useSelector(getArgeementText)
  const [about, setAbout] = useState('')
  const [agreement, setAgreement] = useState('')
  const [editText, setEditText] = useState({
    about: false,
    agreement: false,
  })

  useEffect(() => {
    dispatch(fetchTextsFromServer())
  }, [])
  
  useEffect(() => {
    setAbout(aboutTextDefault || '')
    setAgreement(agreementTextDefault || '')
  },[aboutTextDefault, agreementTextDefault])

  const save = (type = 'about') => () => {
    const state = { about, agreement }
    let data = null
    if (type === 'about') {
      if (!about) return
      
      data = about
    } else {
      if (!agreement) return
      
      data = agreement
    }
    state[type] = data
    dispatch(addTexts(state))
    
    const editState = {...editText}
    editState[type] = false
    setEditText(editState)
  }

  const edit = (type = 'about') => () => {
    const state = { ...editText }
    state[type] = true
    setEditText(state)
  }

  const cancelHandler = (type = 'about') => () => {
    const editState = {...editText}
    editState[type] = false
    setEditText(editState)
  
    if (type === 'about') {
      setAbout(aboutTextDefault)
    } else {
      setAgreement(agreementTextDefault)
    }
  }

  const saveTextValue = (value, type = 'about') => {
    if (type === 'about') {
      setAbout(value)
    } else {
      setAgreement(value)
    }
  }
  
  return (
    <>
    <h2>О проекте</h2>
    <Grid item xs={8}>
      <TextField
        variant="outlined"
        fullWidth
        multiline
        value={about}
        disabled={!editText['about']}
        onChange={(event) => saveTextValue(event.target.value, 'about')}
      />
      <Grid item xs={4}>
      <Button
        variant="contained"
        color="primary"
        onClick={editText['about'] ? save('about') : edit('about')}
        style={{ marginTop: 20 }}
        fullWidth
      >
        {editText['about'] ? 'Сохранить' : 'Редактировать'}
      </Button>
      {editText['about'] ? (
          <Button
            variant="contained"
            color="primary"
            onClick={cancelHandler('about')}
            style={{ marginTop: 20 }}
            fullWidth
          >
            Отмена
          </Button>
      ) : null}
      </Grid>
    </Grid>
    
    <h2>Пользовательское соглашение</h2>
    <Grid item xs={8}>
      <TextField
        variant="outlined"
        fullWidth
        multiline
        value={agreement}
        disabled={!editText['agreement']}
        onChange={(event) => saveTextValue(event.target.value, 'agreement')}
      />
      <Grid item xs={4}>
      <Button
        variant="contained"
        color="primary"
        onClick={editText['agreement'] ? save('agreement') : edit('agreement')}
        style={{ marginTop: 20 }}
        fullWidth
      >
        {editText['agreement'] ? 'Сохранить' : 'Редактировать'}
      </Button>
      {editText['agreement'] ? (
          <Button
            variant="contained"
            color="primary"
            onClick={cancelHandler('agreement')}
            style={{ marginTop: 20 }}
            fullWidth
          >
            Отмена
          </Button>
      ) : null}
      </Grid>
    </Grid>
    </>
  ) 
}

export default EditTexts