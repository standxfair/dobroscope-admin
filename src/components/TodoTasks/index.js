import React, { useEffect, useState, useRef } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { DataGrid } from '@material-ui/data-grid';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { Button } from '@material-ui/core';
import { useDispatch, useSelector } from "react-redux";
import { v4 as uuid } from "uuid";
import { fetchTodosFromServer, addTodos } from '../../ducks/todos'
import ConfirmDialog from '../ui/ConfirmDialog'
import CSVRead from '../common/CSVRead'
import { parseCSVTodosList } from '../../helpers/parseCSV'
import { todoItem } from './constants'

const columns = [
  { field: 'title', headerName: 'Текст задания', width: 430 },
  { field: 'titleEng', headerName: 'English version', width: 430 },
];

export default function TodoTasks() {
  const dispatch = useDispatch();
  const [selectedItems, setSelectedItems] = useState([])
  const [confirmOpen, setConfirmOpen] = useState(false)
  const [editedItem, setEditedItem] = useState(null)
  const [todoValue, setTodoValue] = useState('')
  const [todoValueEng, setTodoValueEng] = useState('')
  const [todosCSVData, setTodosCSVData] = useState([])
  const [todosConfirmOpen, setTodosConfirmOpen] = useState(false)
  const todosDict = useSelector(state => state.todos?.todosDict)
  const loading = useSelector(state => state.todos?.loading)

  const bottomForm = useRef(null)
  const addFileInput = useRef(null)

  const addHandler = (e) => {
    if (todoValue) {
      const elem = { ...todoItem }
      elem.id = uuid()
      elem.title = todoValue
      elem.author = 'Admin'
      elem.titleEng = todoValueEng || ''

      let newTodos = [...todosDict]
      newTodos.push(elem)

      dispatch(addTodos(newTodos))
      setTodoValue('')
    }
  }

  const deleteHandler = () => {
    let newTodos = []
    for (let todo of todosDict) {
      if (!selectedItems.includes(todo.id)) {
        newTodos.push(todo)
      }
    }

    dispatch(addTodos(newTodos))
    setSelectedItems([])
  }

  useEffect(() => {
    if (!todosDict) {
      dispatch(fetchTodosFromServer())
    }
  }, [todosDict, dispatch])

  const editClick = () => {
    bottomForm.current?.scrollIntoView({ behavior: "smooth" })

    const elem = todosDict.find((item) => item.id === selectedItems[0])
    setEditedItem(elem)
    setTodoValue(elem.title)
  }

  const editHandler = () => {
    let newTodos = []
    for (let todo of todosDict) {
      if (todo.id === editedItem.id) {
        todo.title = todoValue
      }
      newTodos.push(todo)
    }
    dispatch(addTodos(newTodos))

    setSelectedItems([])
    setTodoValue('')
    setEditedItem(null)
  }

  const openAddFromFileConfirm = () => {
    if (todosCSVData.length) {
      setTodosConfirmOpen(true)
    }
  }

  const addTodosFromFile = () => {
    if (!todosCSVData.length) return

    const data = parseCSVTodosList(todosCSVData)
    const newTodos = [...todosDict]
    data.forEach(todo => {
      const elem = { ...todoItem }
      elem.id = uuid()
      elem.title = todo.title
      elem.author = todo.author || ''
      elem.titleEng = todo.titleEng || ''
      newTodos.push(elem)
    })
    dispatch(addTodos(newTodos))
    setTodosCSVData([])
    if (addFileInput.current) {
      addFileInput.current.removeFile()
    }
  }

  const onRemoveFile = () => {
    setTodosCSVData([])
  }

  if (loading || !todosDict) {
    return <CircularProgress />
  }

  return (
    <>
      <h1>База Заданий</h1>
      <div style={{ height: 800, width: '100%' }}>
        <DataGrid
          autoHeight={true}
          rows={todosDict}
          columns={columns}
          pageSize={10}
          checkboxSelection
          onSelectionChange={(e) => setSelectedItems(e.rowIds)}
        />
      </div>

      <Grid item xs={12} md={3}>
        {selectedItems.length === 1 ? (
          <Button variant="contained" fullWidth color="primary" onClick={() => editClick()}>
            Редактировать
          </Button>
        ) : null}

        {selectedItems.length ? <div style={{ paddingTop: 20 }}>
          <Button variant="contained" fullWidth color="secondary" onClick={() => setConfirmOpen(true)}>
            Удалить выбранные
          </Button>
          <ConfirmDialog
            open={confirmOpen}
            setOpen={setConfirmOpen}
            onConfirm={deleteHandler}
          >
            Вы уверены что хотите удалить данные без возможности восстановления?
          </ConfirmDialog>
        </div> : null}
      </Grid>

      <Grid container spacing={3} style={{ paddingTop: 20 }} ref={bottomForm}>
        <Grid item xs={12}>
          {editedItem ? <h2>Редактировать</h2> : <h2>Добавить элемент</h2>}
        </Grid>
        <Grid item xs={12}>
          <TextField
            variant="outlined"
            label={editedItem ? '' : 'Текст задания'}
            fullWidth
            value={todoValue}
            onChange={(event) => setTodoValue(event.target.value)}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            variant="outlined"
            label={editedItem ? '' : 'English version'}
            fullWidth
            value={todoValueEng}
            onChange={(event) => setTodoValueEng(event.target.value)}
          />
        </Grid>
        <Grid item xs={12} md={3}>
          <Button
            variant="contained"
            color="primary"
            onClick={editedItem ? editHandler : addHandler}
            style={{ marginTop: 20 }}
            fullWidth
          >
            {editedItem ? 'Сохранить' : 'Добавить'}
          </Button>

          {editedItem ? <Button
            variant="contained"
            color="secondary"
            onClick={() => {
              setEditedItem(null)
              setTodoValue('')
            }}
            style={{ marginTop: 20 }}
            fullWidth
          >
            Отмена
          </Button> : null}
        </Grid>
      </Grid>

      <Grid container spacing={3} style={{ paddingTop: 20 }}>
        <Grid item xs={12}>
          <h2>Загрузить задания из CSV-файла</h2>
        </Grid>
        <Grid item xs={12} md={3}>
          <CSVRead onFileAdd={setTodosCSVData} reference={addFileInput} onRemoveFile={onRemoveFile} />
          <Button
            variant="contained"
            color="primary"
            onClick={openAddFromFileConfirm}
            style={{ marginTop: 20 }}
            fullWidth
          >
            {'Добавить'}
          </Button>
          {todosCSVData.length ? <ConfirmDialog
            open={todosConfirmOpen}
            setOpen={setTodosConfirmOpen}
            onConfirm={addTodosFromFile}
            cancelButtonText='Отмена'
            submitButtonText='OK'
          >
            Добавить задания из файла?
          </ConfirmDialog> : null}
        </Grid>
      </Grid>
    </>
  );
}
