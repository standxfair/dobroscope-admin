import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import CircularProgress from '@material-ui/core/CircularProgress';
import { DataGrid } from '@material-ui/data-grid';
import { Button } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import moment from 'moment'
import { deleteClient, fetchClientsFromServer, updateUsersLocal, saveUserToApi } from '../../ducks/users'
import ConfirmDialog from '../ui/ConfirmDialog'
import EditUser from './EditUser'
import useUser from './useUser'

const columns = [
  { field: 'displayName', headerName: 'Имя', width: 230 },
  { field: 'totalCounter', headerName: 'Выполнено заданий', width: 100 },
  { field: 'totalPosts', headerName: 'Всего постов', width: 100 },
  { field: 'totalLikes', headerName: 'Всего лайков', width: 100 },
  { field: 'email', headerName: 'Email', width: 200 },
  { field: 'agreeReciveEmails', headerName: 'Согласен получать email-рассылку', width: 100, valueFormatter: ({ value }) => (value ? 'Да' : 'Нет') },
  { field: 'agreeRecivePush', headerName: 'Согласен получать push-уведомления', width: 100, valueFormatter: ({ value }) => (value ? 'Да' : 'Нет') },
  { field: 'lastLoginTime', headerName: 'Последний вход', width: 200, valueFormatter: ({ value }) => moment(value).format('DD.MM.YYYY HH:mm') },
];

export default function Users() {
  const [deletedItems, setDeletedItems] = useState([])
  const [confirmOpen, setConfirmOpen] = useState(false)
  const [deleteConfirmOpen, setDeleteConfirmOpen] = useState(false)
  const [editedUserId, setEditedUserId] = useState(null)

  const users = useSelector(state => state.users?.clients)
  const loading = useSelector(state => state.users?.loading)
  const user = useUser(editedUserId)
  const dispatch = useDispatch();

  useEffect(() => {
    if (!users) {
      dispatch(fetchClientsFromServer())
    }
  }, [users, dispatch])

  const deleteHandler = () => {
    for (let id of deletedItems) {
      dispatch(deleteClient(id))
    }
    setDeletedItems([])
  }

  const saveUserChanges = ({ achievType }) => {
    if (achievType) {
      const updatedUsers = [...users]
      updatedUsers.forEach(user => {
        if (user.id === editedUserId) {
          if (user.achievements) {
            user.achievements.push(achievType)
          } else {
            user.achievements = [achievType]
          }
        }
      })
      dispatch(updateUsersLocal(updatedUsers))
    }
  }

  const saveUser = () => {
    dispatch(saveUserToApi(editedUserId))
    setEditedUserId(null)
  }

  if (loading || !users) {
    return <CircularProgress />
  }

  return (
    <>
      <h1>Пользователи</h1>
      <div style={{ minHeight: 400, width: '100%' }}>
        <DataGrid
          autoHeight={true}
          rows={users}
          columns={columns}
          pageSize={20}
          onSelectionChange={(e) => setEditedUserId(e.rowIds[0])} />
      </div>
      {editedUserId ? <div style={{ paddingTop: 20 }}>
        <Grid container spacing={6}>
          <Grid item xs={2}>
            <Button
              variant="contained"
              color="primary"
              onClick={() => setConfirmOpen(true)}
            >
              Редактировать
            </Button>
            <ConfirmDialog
              open={confirmOpen}
              setOpen={setConfirmOpen}
              onConfirm={saveUser}
              fullWidth={true}
              submitButtonText="Сохранить"
            >
              <EditUser
                saveData={saveUserChanges}
                user={user}
              />
            </ConfirmDialog>
          </Grid>

          <Grid item xs={3}>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => setDeleteConfirmOpen(true)}
            >
              Удалить
            </Button>
            <ConfirmDialog
              open={deleteConfirmOpen}
              setOpen={setDeleteConfirmOpen}
              onConfirm={deleteHandler}
            >
              Вы уверены, что хотите удалить пользователя?
            </ConfirmDialog>
          </Grid>
        </Grid>
      </div> : null}
    </>
  );
}
