import React from 'react';
import { useFormik } from 'formik';
import * as yup from 'yup';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { Button } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import { types } from './constants'

const validationSchema = yup.object({
  title: yup
    .string('Введите текст')
    .required('Поле обязательно'),
  type: yup
    .string('Укажите тип')
    .required('Поле обязательно'),
});

export default function AddItemForm({ submit, editedItem, setEditedItem, editHandler }) {
  const formik = useFormik({
    initialValues: {
      title: editedItem ? editedItem.title : '',
      author: editedItem ? editedItem.author : '',
      titleEng: editedItem ? editedItem.titleEng : '',
      authorEng: editedItem ? editedItem.authorEng : '',
      type: editedItem ? editedItem.type : '',
    },
    validationSchema: validationSchema,
    onSubmit: (values, e) => {
      if (editedItem) {
        editHandler(values)
      } else {
        submit(values)
      }
      e.resetForm({})
    },
    enableReinitialize: true,
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="title"
        label="Текст"
        name="title"
        autoComplete="off"
        value={formik.values.title}
        onChange={formik.handleChange}
        error={formik.touched.title || Boolean(formik.errors.title)}
        helperText={formik.touched.title || formik.errors.title}
      />

      <TextField
        variant="outlined"
        margin="normal"
        fullWidth
        id="author"
        label="Автор"
        name="author"
        autoComplete="off"
        value={formik.values.author}
        onChange={formik.handleChange}
        error={formik.touched.author || Boolean(formik.errors.author)}
        helperText={formik.touched.author || formik.errors.author}
      />

      <TextField
        variant="outlined"
        margin="normal"
        fullWidth
        id="titleEng"
        label="Title in english"
        name="titleEng"
        autoComplete="off"
        value={formik.values.titleEng}
        onChange={formik.handleChange}
        error={formik.touched.titleEng || Boolean(formik.errors.titleEng)}
        helperText={formik.touched.titleEng || formik.errors.titleEng}
      />

      <TextField
        variant="outlined"
        margin="normal"
        fullWidth
        id="authorEng"
        label="Author in english"
        name="authorEng"
        autoComplete="off"
        value={formik.values.authorEng}
        onChange={formik.handleChange}
        error={formik.touched.authorEng || Boolean(formik.errors.authorEng)}
        helperText={formik.touched.authorEng || formik.errors.authorEng}
      />

      <FormControl variant="outlined" style={{ minWidth: 120, }}>
        <InputLabel id="type-label">Тип</InputLabel>
        <Select
          labelId="type-label"
          id="type"
          value={formik.values.type}
          onChange={formik.handleChange}
          label="Тип"
          name="type"
          error={formik.touched.type || Boolean(formik.errors.type)}
        >
          {types.map((item, key) => <MenuItem value={item} key={key}>{item}</MenuItem>)}
        </Select>
      </FormControl>
      <Grid item xs={12} md={3}>
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          style={{ marginTop: 20 }}
        >
          {editedItem ? 'Сохранить' : 'Добавить'}
        </Button>

        {editedItem ? <Button
          variant="contained"
          color="secondary"
          onClick={() => {
            setEditedItem(null)
            formik.resetForm({})
          }}
          style={{ marginTop: 20 }}
          fullWidth
        >
          Отмена
        </Button> : null}
      </Grid>
    </form>
  );
}
