import { combineReducers } from "redux";
import authReducer, { moduleName as authModule } from "../ducks/auth";
import todosReducer, { moduleName as todosModule } from "../ducks/todos";
import motivationReducer, { moduleName as motivationModule } from "../ducks/motivation";
import feedReducer, { moduleName as feedModule } from "../ducks/feed";
import messagesReducer, { moduleName as messagesModule } from "../ducks/messages";
import usersReducer, { moduleName as usersModule } from "../ducks/users";
import textsReducer, { moduleName as textsModule } from "../ducks/texts";
import achievsReducer, { moduleName as achievsModule } from "../ducks/achievements";

export default combineReducers({
  [authModule]: authReducer,
  [todosModule]: todosReducer,
  [motivationModule]: motivationReducer,
  [feedModule]: feedReducer,
  [messagesModule]: messagesReducer,
  [usersModule]: usersReducer,
  [textsModule]: textsReducer,
  [achievsModule]: achievsReducer,
});
