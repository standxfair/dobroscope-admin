import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { useDispatch, useSelector } from "react-redux";
import Select from '@material-ui/core/Select';
import { fetchAchievsFromServer } from '../../ducks/achievements'
// import CloseIcon from '@material-ui/icons/Close';

const EditUser = ({ user, saveData }) => {
  const [currentAchiev, setCurrentAchiev] = useState("")
  const dispatch = useDispatch();
  const achievements = useSelector(state => state.achievements?.achievements)

  useEffect(() => {
    dispatch(fetchAchievsFromServer())
  }, [])

  const handleChangeAchiev = (event) => {
    setCurrentAchiev(event.target.value)
    if (!user.achievements || !user.achievements.includes(event.target.value)) {
      saveData({ [event.target.name]: event.target.value })
    }
  }

  const getAchievTitle = (id) => {
    return achievements.find(item => item.id === id)?.title
  }

  // const removeAchiveHandler = (id) => () => {
  //   if (user.achievements || user.achievements.includes(id)) {
  //     let result = []
  //     user.achievements.forEach(item => {
  //       if (item !== id) {
  //         result.push(item)
  //       }
  //     })

  //   }
  // }

  if (!achievements) return null

  return (
    <>
      <Grid container spacing={6}>
        <Grid item xs={6}>
          <h2>Текущие награды</h2>
          {user?.achievements.length
            ? <ul>
              {user.achievements.map((id) =>
                <li key={id}>
                  {getAchievTitle(id)}
                </li>)}
            </ul>
            : 'Нет наград'}
        </Grid>
        <Grid item xs={6}>
          {achievements
            ? <>
              <h2>Установить награду</h2>
              <Select
                native
                value={currentAchiev}
                onChange={handleChangeAchiev}
                inputProps={{
                  name: 'achievType',
                }}
              >
                <option aria-label="None" value="" />
                {achievements.map((item => <option value={item.id}>{item.title}</option>))}
              </Select>
            </>
            : null}
        </Grid>
      </Grid>

      {/* <h2>Установить награду</h2>
      <Grid xs={12}>
        <TextField
          variant="outlined"
          fullWidth
          value={title}
          onChange={(event) => changeHandler(event.target.value, 'title')}
        />
      </Grid>

      {image ? <Grid xs={12}>
        <img src={image} alt="" />
      </Grid>
        : null}

      {!image ? <Grid xs={12}>
        <Button
          variant="contained"
          component="label"
        >
          Загрузить изображение
          <input
            type="file"
            hidden
            onChange={(ev) => changeHandler(ev.target, 'image')}
          />
        </Button>
      </Grid> : null}

      {image ? <Grid xs={12}>
        <Button
          variant="contained"
          component="label"
          onClick={deleteImageHandler}
        >
          Удалить изображение
        </Button>
      </Grid> : null} */}
    </>
  )
}

export default EditUser