import React, { useEffect, useState } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import CloseIcon from '@material-ui/icons/Close';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Button from '@material-ui/core/Button';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Tooltip from '@material-ui/core/Tooltip';
import Badge from '@material-ui/core/Badge';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import VisibilityIcon from '@material-ui/icons/Visibility';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import { useDispatch, useSelector } from "react-redux";
import { fetchFeedFromServer, getSortedFeed, getAvailibleDates, deleteFromFeed } from '../../ducks/feed'
import { getFeedItemsByDay } from './helpers'
import ConfirmDialog from '../ui/ConfirmDialog'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    overflow: 'hidden',
    padding: theme.spacing(0, 3),
  },
  paper: {
    // maxWidth: 400,
    margin: `${theme.spacing(1)}px auto`,
    padding: theme.spacing(2),
  },
  itemHeader: {
    // background: '#eee',
    alignItems: 'center',
    marginBottom: theme.spacing(2),
  },
  date: {
    fontSize: 12,
    marginBottom: -20,
    color: '#837070'
  },
  showComment: {
    cursor: 'pointer'
  },
  img: {
    objectFit: 'cover',
    width: '100%',
    maxHeight: 200,
  },
  commentsCont: {
    marginTop: -30,
  }

}));

export default function Feed() {
  const dispatch = useDispatch();
  const feed = useSelector(getSortedFeed)
  const dates = useSelector(getAvailibleDates)
  const loading = useSelector(state => state.feed?.loading)

  const [value, setValue] = useState(0);
  const [currentFeed, setCurrentFeed] = useState(null)
  const [currentDate, setCurrentDate] = useState(null)

  useEffect(() => {
    if (!feed) {
      dispatch(fetchFeedFromServer())
    }

    if (dates && !currentDate) {
      setCurrentDate(dates[0])
    }

    if (feed) {
      let feed_
      if (value === 0) {
        feed_ = getFeedItemsByDay(feed, currentDate)
      } else {
        feed_ = [...feed]
      }
      setCurrentFeed(feed_)
    }
  }, [currentDate, feed, value, dispatch, dates])

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const deleteHandler = (itemId, commentId) => () => {
    dispatch(deleteFromFeed(itemId, commentId))
  }

  if (loading || !feed || !dates || !currentFeed) {
    return <CircularProgress />
  }

  return (
    <>
      <Tabs
        value={value}
        onChange={handleChange}
        indicatorColor="primary"
        textColor="primary"
        centered
      >
        <Tab label="Лента по дням" />
        <Tab label="Вся лента" />
      </Tabs>
      { value === 0 ? (
        <SelectDay
          dates={dates}
          currentDate={currentDate}
          setCurrentDate={setCurrentDate}
        />
      ) : null}

      {currentFeed.map((item, key) => <FeedItem item={item} key={key} deleteHandler={deleteHandler} />)}
    </>
  );
}


const FeedItem = ({ item, deleteHandler }) => {
  const classes = useStyles();
  const [showComments, setShowComments] = useState(true)
  const [confirmCommentOpen, setConfirmCommentOpen] = useState(false)
  const [confirmOpen, setConfirmOpen] = useState(false)

  const showCommentsHandler = () => setShowComments(!showComments)

  return (
    <>
      <Paper className={classes.paper}>
        <Grid container spacing={3} className={classes.itemHeader}>
          <Grid item xs={12}>
            <div className={classes.date}>{item.createDate}</div>
          </Grid>

          <Avatar
            src={item.avatar || null}
            alt="item.author"
            style={{ marginLeft: 10 }}
          >
            {!item.avatar ? item.author[0] : null}
          </Avatar>

          <Grid item md={9} xs={6}>
            <div>{item.author}</div>
          </Grid>

          {item.likes ?
            <Grid item xs={1}>
              <Badge badgeContent={item.likes.length} color="primary"><FavoriteIcon color="secondary" /></Badge>
            </Grid> : null}

          <Grid item xs={1}>
            <Tooltip title="Удалить элемент из ленты" placement="top">
              <Button color="secondary" onClick={() => setConfirmOpen(true)}>
                <HighlightOffIcon />
              </Button>
            </Tooltip>
            <ConfirmDialog
              open={confirmOpen}
              setOpen={setConfirmOpen}
              onConfirm={deleteHandler(item.id)}
            >
              Вы уверены что хотите удалить данные без возможности восстановления?
                </ConfirmDialog>
          </Grid>
          <Grid item xs={8}>
            {item.text}
          </Grid>
          {item.imgUrl ? <Grid item xs={4}>
            <img src={item.imgUrl} alt='' className={classes.img} />
          </Grid> : null}
        </Grid>

        {item.comments ? (
          <Grid container spacing={1} className={classes.commentsCont}>
            <Grid item xs={12}>
              <span onClick={showCommentsHandler} className={classes.showComment}>
                <h4 style={{
                  display: 'flex',
                  alignItems: 'center',
                  flexWrap: 'wrap',
                }}>{
                    showComments ? 'Скрыть комментарии ' : 'Показать комментарии  '
                  } {showComments ? <VisibilityOffIcon fontSize="small" /> : <VisibilityIcon fontSize="small" />}</h4>
              </span>

            </Grid>
            <List style={{ width: '100%' }}>
              {showComments ? item.comments.map((comment, key) => (
                <React.Fragment key={key}>
                  <ListItem >
                    <ListItemText primary={<>
                      {comment.author} <span style={{ fontSize: 12, paddingLeft: 20 }}>{comment.createDate}</span>
                    </>} secondary={comment.text}> </ListItemText>

                    <Button color="secondary" onClick={() => setConfirmCommentOpen(true)}>
                      <CloseIcon />
                    </Button>

                    <ConfirmDialog
                      open={confirmCommentOpen}
                      setOpen={setConfirmCommentOpen}
                      onConfirm={deleteHandler(item.id, comment.id)}
                    >
                      Вы уверены что хотите удалить данные без возможности восстановления?
                </ConfirmDialog>
                  </ListItem>
                  <Divider />
                </React.Fragment>
              )) : null}
            </List>
          </Grid>
        ) : null}
      </Paper>


    </>
  )
}

const SelectDay = ({ dates, currentDate, setCurrentDate }) => {
  if (dates) {
    return (
      <>
        <Select
          labelId="type-label"
          id="type"
          value={currentDate}
          onChange={(ev) => setCurrentDate(ev.target.value)}
          label="Тип"
          name="type"
        >
          {dates.map((item, key) => <MenuItem value={item} key={key}>{item}</MenuItem>)}
        </Select>
      </>
    )
  }
}