import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import DashboardIcon from '@material-ui/icons/Dashboard';
import PeopleIcon from '@material-ui/icons/People';
import DynamicFeedIcon from '@material-ui/icons/DynamicFeed';
import FeedbackIcon from '@material-ui/icons/Feedback';
import AssignmentIcon from '@material-ui/icons/Assignment';
import EditIcon from '@material-ui/icons/Edit';
import StarsIcon from '@material-ui/icons/Stars';

export const MainListItems = ({ setCurrentScreen }) => (
  <div>
    <ListItem button onClick={() => setCurrentScreen('main')}>
      <ListItemIcon>
        <DashboardIcon />
      </ListItemIcon>
      <ListItemText primary="Главная" />
    </ListItem>

    <ListItem button onClick={() => setCurrentScreen('users')}>
      <ListItemIcon>
        <PeopleIcon />
      </ListItemIcon>
      <ListItemText primary="Пользователи" />
    </ListItem>

    <ListItem button onClick={() => setCurrentScreen('feed')}>
      <ListItemIcon>
        <DynamicFeedIcon />
      </ListItemIcon>
      <ListItemText primary="Лента дел" />
    </ListItem>

    <ListItem button onClick={() => setCurrentScreen('messages')}>
      <ListItemIcon>
        <FeedbackIcon />
      </ListItemIcon>
      <ListItemText primary="Обратная связь" />
    </ListItem>

    <ListItem button onClick={() => setCurrentScreen('editTexts')}>
      <ListItemIcon>
        <EditIcon />
      </ListItemIcon>
      <ListItemText primary="Редактировать тексты" />
    </ListItem>
  </div>
)

export const SecondaryListItems = ({ setCurrentScreen }) => (
  <div>
    <ListSubheader inset>Базы</ListSubheader>
    <ListItem button onClick={() => setCurrentScreen('todos')}>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Задания" />
    </ListItem>

    <ListItem button onClick={() => setCurrentScreen('motivation')}>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Мотивация" />
    </ListItem>

    <ListItem button onClick={() => setCurrentScreen('achievements')}>
      <ListItemIcon>
        <StarsIcon />
      </ListItemIcon>
      <ListItemText primary="Награды" />
    </ListItem>
  </div>
)