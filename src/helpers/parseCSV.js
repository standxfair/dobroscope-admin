export const parseCSVTodosList = (csvTodos) => {
  let result = []
  csvTodos.forEach((todo, key) => {
    if (key !== 0) {
      const { data } = todo
      if (data[1]) {
        result.push({
          title: data[1],
          author: data[2] || '',
          titleEng: data[3] || '',
        })
      }
    }
  })
  return result
}

export const parseCSVMotivationList = (csvTodos) => {
  let result = []
  csvTodos.forEach((todo, key) => {
    if (key !== 0) {
      const { data } = todo
      if (data[1]) {
        result.push({
          type: data[1] || '',
          title: data[2],
          author: data[3] || '',
          titleEng: data[4],
          authorEng: data[5] || ''
        })
      }
    }
  })
  return result
}