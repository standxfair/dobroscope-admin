import React, { useEffect } from 'react';
import { useSelector, useDispatch } from "react-redux";
import clsx from 'clsx';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import ListItemText from '@material-ui/core/ListItemText';
import { drawerWidth } from '../../constants/app'
import { getSortedFeed, fetchFeedFromServer } from '../../ducks/feed'
import { fetchFeedbackFromServer, getSortedNewMessages } from '../../ducks/messages'
import { fetchClientsFromServer } from '../../ducks/users'
import { IState, IFeedItem, IMessagesItem } from '../../interfaces'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
}));

const FEED_SIZE: number = 5
const MESSAGES_SIZE: number = 5

type MainScreenProps = {
  setCurrentScreen: (screenName: string) => void
}

export default function Main(props: MainScreenProps) {
  const feed = useSelector(getSortedFeed)
  const messages = useSelector(getSortedNewMessages)
  const users = useSelector((state: IState) => state.users?.clients)
  const dispatch = useDispatch();

  const classes = useStyles();
  const fixedHeightPaper = clsx(classes.paper);

  useEffect(() => {
    if (!feed) {
      dispatch(fetchFeedFromServer())
    }
    if (!messages) {
      dispatch(fetchFeedbackFromServer())
    }
    if (!users) {
      dispatch(fetchClientsFromServer())
    }
  }, [feed, dispatch, messages, users])

  return (
    <Grid container spacing={3}>
      <Grid item md={9} xs={12}>
        <Paper className={fixedHeightPaper}>
          <h3>Последние записи в Ленте дел</h3>
          {feed ? (
            <>
              <FeedMain feed={feed} />
              <Link
                component="button"
                variant="body2"
                onClick={() => props.setCurrentScreen('feed')}
              >
                Подробнее
          </Link>
            </>
          ) : 'Нет записей в ленте'}
        </Paper>
      </Grid>

      <Grid item md={3} xs={12} >
        <Paper className={fixedHeightPaper}>
          <h3>Всего зарегистрированных пользователей:</h3>
          {!users ? <CircularProgress /> : <h2>{users.length}</h2>}

          <Link
            component="button"
            variant="body2"
            onClick={() => props.setCurrentScreen('users')}
          >
            Пользователи
          </Link>
        </Paper>
      </Grid>
      <Grid item xs={12} >
        <Paper className={fixedHeightPaper}>
          <h3>Новые сообщения</h3>
          {messages?.length ? (
            <MessagesMain messages={messages} />
          ) : 'Нет новых сообщений'}
          <Link
            component="button"
            variant="body2"
            onClick={() => props.setCurrentScreen('messages')}
          >
            Все сообщения
              </Link>
        </Paper>
      </Grid>
    </Grid>
  );
}

type FeedProps = {
  feed: IFeedItem[]
}
const FeedMain: React.FC<FeedProps> = ({ feed }) => {
  return (
    <List dense={true}>
      {feed.slice(0, FEED_SIZE).map((item, key) => (
        <ListItem key={key}>
          <ListItemAvatar>
            {item.avatar ? (
              <Avatar
                src={item.avatar}
                alt="item.author"
              />
            ) : (
                <Avatar
                  alt="item.author"
                >
                  {item.author[0]}
                </Avatar>
              )}

          </ListItemAvatar>
          <ListItemText
            primary={item.author + ' | ' + item.createDate}
            secondary={item.text}
          />
        </ListItem>
      ))}

    </List>
  )
}

type MessagesProps = {
  messages: IMessagesItem[]
}

const MessagesMain: React.FC<MessagesProps> = ({ messages }) => {
  return (
    <List dense={true}>
      {messages.slice(0, MESSAGES_SIZE).map((item, key) => (
        <ListItem key={key}>
          <ListItemAvatar>
            <Avatar
            >
              {item.userName[0]}
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary={item.userName + ' | ' + item.createDate}
            secondary={item.message.slice(0, 200) + '...'}
          />
        </ListItem>
      ))}
    </List>
  )
}