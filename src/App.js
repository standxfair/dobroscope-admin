import 'fontsource-roboto';
import { Route, Switch, Redirect, } from "react-router-dom";
import LoginForm from './components/LoginForm'
import Dashboard from './components/Dashboard'
import { useAdmin, userIdSelector, useAuthorized } from './ducks/auth'
import { useSelector } from "react-redux";
import CallToAdmin from './components/CallToAdmin'

function App() {
  return (
    <div>
      <Switch>
        <LoginRoute exact path='/'>
          <LoginForm />
        </LoginRoute>
        <PrivateRoute path='/admin'>
          <Dashboard />
        </PrivateRoute>
      </Switch>
    </div>
  );
}

export default App;


function LoginRoute({ children, ...rest }) {
  const admin = useAdmin()
  const user = useAuthorized()
  const userId = useSelector(userIdSelector)

  return (
    <Route
      {...rest}
      render={({ location }) =>
        !admin ? (
          user ? (
            <CallToAdmin userId={userId} />
          ) : (
              children
            )
        ) : (
            <Redirect
              to={{
                pathname: "/admin",
                state: { from: location }
              }}
            />
          )
      }
    />
  );
}

function PrivateRoute({ children, ...rest }) {
  const admin = useAdmin()
  const user = useAuthorized()
  const userId = useSelector(userIdSelector)

  return (
    <Route
      {...rest}
      render={({ location }) =>
        admin ? (
          children
        ) : (
            user ? (
              <CallToAdmin userId={userId} />
            ) : (
                <Redirect
                  to={{
                    pathname: "/",
                    state: { from: location }
                  }}
                />
              )
          )
      }
    />
  );
}

