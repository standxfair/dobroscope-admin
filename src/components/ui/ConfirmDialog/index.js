import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

const ConfirmDialog = ({ title, children, open, setOpen, onConfirm, cancelButtonText = 'Отмена', submitButtonText = 'Удалить', fullWidth = false, onCancel = () => { } }) => {

  return (
    <Dialog
      open={open}
      onClose={() => setOpen(false)}
      aria-labelledby="confirm-dialog"
      fullWidth={fullWidth}
    >
      <DialogTitle id="confirm-dialog">{title}</DialogTitle>
      <DialogContent>{children}</DialogContent>
      <DialogActions>
        <Button
          variant="contained"
          onClick={() => {
            onCancel()
            setOpen(false)
          }}
          color="default"
        >
          {cancelButtonText}
        </Button>
        <Button
          variant="contained"
          onClick={() => {
            setOpen(false);
            onConfirm();
          }}
          color="secondary"
        >
          {submitButtonText}
        </Button>
      </DialogActions>
    </Dialog>
  );
};
export default ConfirmDialog;