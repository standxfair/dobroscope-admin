export const getFeedItemsByDay = (feed, date) => {
  let result = []
  for (const item of feed) {
    if (item.createDate === date) {
      result.push(item)
    }
  }
  return result
}