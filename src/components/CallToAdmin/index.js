import React, { useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import { useSelector } from "react-redux";
import apiService from '../../services/api'
import BackLoader from '../ui/BackLoader'


export default function CallToAdmin({ userId }) {
  const [loading, setLoading] = useState(true)
  const isNewUser = useSelector(state => state.auth.isNewUser)

  useEffect(() => {
    setTimeout(() => {
      setLoading(true)
    }, 800)
  }, [])

  if (loading && !isNewUser) {
    return (
      <BackLoader open={loading} />
    )
  }

  return (
    <Grid container spacing={3}>
      <Grid item xs={3}></Grid>
      <Grid item xs={6}>
        <div>Для получения доступа в админ-панель обтратитесь к администратору.</div>
        <div>Ваш id: <b>{userId}</b></div>
        <div>
          <Link
            component="button"
            variant="body2"
            onClick={() => apiService.signOut()}
          >
            Выйти
          </Link>
        </div>
      </Grid>
    </Grid>
  );
}
