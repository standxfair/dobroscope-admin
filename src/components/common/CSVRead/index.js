import React, { Component } from 'react'

import { CSVReader } from 'react-papaparse'

export default class CSVRead extends Component {
  handleOnDrop = (data) => {
    console.log('---------------------------')
    console.log(data)
    console.log('---------------------------')
    this.props.onFileAdd(data)
  }

  handleOnError = (err, file, inputElem, reason) => {
    console.log(err)
  }

  handleOnRemoveFile = (data) => {
    this.props.onRemoveFile()
    console.log('---------------------------')
    console.log(data)
    console.log('---------------------------')
  }

  render() {
    return (
      <CSVReader
        onDrop={this.handleOnDrop}
        onError={this.handleOnError}
        addRemoveButton
        removeButtonColor='#659cef'
        onRemoveFile={this.handleOnRemoveFile}
        ref={this.props.addFileInput || null}
      >
        <span>Перетащите сюда CSV-файл, или нажмите для загрузки</span>
      </CSVReader>
    )
  }
}