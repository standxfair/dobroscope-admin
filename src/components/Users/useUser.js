import { useEffect, useState } from 'react';
import { useSelector } from "react-redux";

const useUser = (userId) => {
  const clients = useSelector(state => state.users?.clients)
  const [user, setUser] = useState(null)

  useEffect(() => {
    if (!userId) return

    const currentUser = clients.find((client => client.id === userId))
    setUser(currentUser)
  }, [clients, userId])

  return user
}

export default useUser