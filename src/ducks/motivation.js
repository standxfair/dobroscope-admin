import { all, call, put, take, takeEvery, } from "redux-saga/effects";
import { eventChannel } from "redux-saga";
// import { createSelector } from "reselect"
import { appName } from "../config";
import { Record } from "immutable";
import apiService from "../services/api";
// import { useSelector } from "react-redux";

/**
 * Constants
 * */
export const moduleName = "motivation";
const prefix = `${appName}/${moduleName}`;


export const FETCH_MOTIVATION_REQUEST = `${prefix}/FETCH_MOTIVATION_REQUEST`;
export const FETCH_MOTIVATION_SUCCESS = `${prefix}/FETCH_MOTIVATION_SUCCESS`;
export const FETCH_MOTIVATION_ERROR = `${prefix}/FETCH_MOTIVATION_ERROR`;

export const ADD_MOTIVATION_REQUEST = `${prefix}/ADD_MOTIVATION_REQUEST`;

/**
 * Reducer
 * */
export const ReducerRecord = Record({
  motivationDict: null,
  loading: false,
  error: null,
});

export default function reducer(state = new ReducerRecord(), action) {
  const { type, payload, error } = action;

  switch (type) {
    case FETCH_MOTIVATION_REQUEST:
      return state.set("loading", true);

    case FETCH_MOTIVATION_SUCCESS:
      return state
        .set("loading", false)
        .set("motivationDict", payload.motivationDict)
        .set("error", null);

    default:
      return state;
  }
}

/**
 * Selectors
 * */

// export const userSelector = (state) => state[moduleName].user;

// const getAdmins = state => state[moduleName]?.admins
// export const isAdmin = createSelector([getAdmins, userSelector], (admins, user) => {
//   if (!!admins && !!user) {
//     return !!admins[user.uid]
//   }
// })

/**
 * Custom Hooks
 */

// export const useAuthorized = () => {
//   const user = useSelector(userSelector);

//   return !!user;
// };

// export const useAdmin = () => {
//   const user = useSelector(isAdmin)

//   return !!user;
// };

/**
 * Action Creators
 * */

export const fetchMotivationFromServer = () => ({
  type: FETCH_MOTIVATION_REQUEST,
});

export const addMotivation = (motivation) => ({
  type: ADD_MOTIVATION_REQUEST,
  payload: motivation
});

/**
 * Sagas
 */

export const createMotivationChanel = () => eventChannel(emit => apiService.fetchMotivation(emit));

export const syncMotivationState = function* () {
  const chanel = yield call(createMotivationChanel);

  while (true) {
    const { motivationDict } = yield take(chanel);

    if (motivationDict) {
      yield put({
        type: FETCH_MOTIVATION_SUCCESS,
        payload: { motivationDict },
      });
    } else {
      yield put({
        type: FETCH_MOTIVATION_ERROR,
      });
    }
  }
};

export const addMotivationSaga = function* ({ payload }) {
  yield call(apiService.updateMotivationDict, payload);
}


export const saga = function* () {
  yield all([takeEvery(FETCH_MOTIVATION_REQUEST, syncMotivationState)]);
  yield all([takeEvery(ADD_MOTIVATION_REQUEST, addMotivationSaga)]);
};
