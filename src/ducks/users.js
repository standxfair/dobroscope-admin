import { all, call, put, take, takeEvery, select } from "redux-saga/effects";
import { eventChannel } from "redux-saga";
import { Record } from "immutable";
import { appName } from "../config";
import apiService from "../services/api";

/**
 * Constants
 * */
export const moduleName = "users";
const prefix = `${appName}/${moduleName}`;

export const FETCH_USERS_REQUEST = `${prefix}/FETCH_USERS_REQUEST`;
export const FETCH_USERS_SUCCESS = `${prefix}/FETCH_USERS_SUCCESS`;
export const FETCH_USERS_ERROR = `${prefix}/FETCH_USERS_ERROR`;
export const ADD_USERS_REQUEST = `${prefix}/ADD_USERS_REQUEST`;
export const DELETE_USER_ITEM = `${prefix}/DELETE_USER_ITEM`;
export const SET_MESSAGE_STATUS = `${prefix}/SET_MESSAGE_STATUS`;
export const UPDATE_USERS = `${prefix}/UPDATE_USERS`;
export const UPDATE_USERS_SUCCESS = `${prefix}/UPDATE_USERS_SUCCESS`;
export const SAVE_USER_REQUEST = `${prefix}/SAVE_USER_REQUEST`;

/**
 * Reducer
 * */
export const ReducerRecord = Record({
  clients: null,
  loading: false,
  error: null,
});

export default function reducer(state = new ReducerRecord(), action) {
  const { type, payload, error } = action;

  switch (type) {
    case FETCH_USERS_REQUEST:
      return state.set("loading", true);

    case FETCH_USERS_SUCCESS:
      return state
        .set("loading", false)
        .set("clients", payload.clients)
        .set("error", null);

    default:
      return state;
  }
}

/**
 * Selectors
 * */

export const clientsSelector = (state) => state[moduleName].clients;

/**
 * Custom Hooks
 */


/**
 * Action Creators
 * */

export const fetchClientsFromServer = () => ({
  type: FETCH_USERS_REQUEST,
});

export const deleteClient = (id) => ({
  type: DELETE_USER_ITEM,
  payload: { id }
});

export const updateUsersLocal = (clients) => ({
  type: UPDATE_USERS,
  payload: { clients }
});

export const saveUserToApi = (id) => ({
  type: SAVE_USER_REQUEST,
  payload: { id }
});

/**
 * Sagas
 */

export const createClientsChanel = () => eventChannel(emit => apiService.fetchClients(emit));

export const syncClietnsState = function* () {
  const chanel = yield call(createClientsChanel);

  while (true) {
    const { clients } = yield take(chanel);
    if (clients) {
      yield put({
        type: FETCH_USERS_SUCCESS,
        payload: { clients },
      });
    } else {
      yield put({
        type: FETCH_USERS_ERROR,
      });
    }
  }
}

export const deleteClientSaga = function* ({ payload }) {
  yield call(apiService.deleteClient, payload);
}

export const saveClientSaga = function* ({ payload }) {
  const users = yield select(clientsSelector)
  const user = users.find(item => item.id === payload.id)
  yield call(apiService.saveClient, user);
}

export const updateUsersSaga = function* ({ payload }) {
  yield put({
    type: UPDATE_USERS_SUCCESS,
    payload,
  })
}

export const saga = function* () {
  yield all([
    takeEvery(FETCH_USERS_REQUEST, syncClietnsState),
    takeEvery(DELETE_USER_ITEM, deleteClientSaga),
    takeEvery(UPDATE_USERS, updateUsersSaga),
    takeEvery(SAVE_USER_REQUEST, saveClientSaga),
  ]);
};
