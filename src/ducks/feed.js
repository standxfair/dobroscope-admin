import { all, call, put, take, takeEvery, } from "redux-saga/effects";
import { eventChannel } from "redux-saga";
import { createSelector } from "reselect"
import { Record } from "immutable";
import moment from 'moment'
import { appName } from "../config";
import apiService from "../services/api";


/**
 * Constants
 * */
export const moduleName = "feed";
const prefix = `${appName}/${moduleName}`;


export const FETCH_FEED_REQUEST = `${prefix}/FETCH_FEED_REQUEST`;
export const FETCH_FEED_SUCCESS = `${prefix}/FETCH_FEED_SUCCESS`;
export const FETCH_FEED_ERROR = `${prefix}/FETCH_FEED_ERROR`;

export const ADD_FEED_REQUEST = `${prefix}/ADD_FEED_REQUEST`;

export const DELETE_FEED_ITEM = `${prefix}/DELETE_FEED_ITEM`;
export const DELETE_FEED_ITEM_COMMENT = `${prefix}/DELETE_FEED_ITEM_COMMENT`;


/**
 * Reducer
 * */
export const ReducerRecord = Record({
  feed: null,
  loading: false,
  error: null,
});

export default function reducer(state = new ReducerRecord(), action) {
  const { type, payload, error } = action;

  switch (type) {
    case FETCH_FEED_REQUEST:
      return state.set("loading", true);

    case FETCH_FEED_SUCCESS:
      return state
        .set("loading", false)
        .set("feed", payload.feed)
        .set("error", null);

    default:
      return state;
  }
}

/**
 * Selectors
 * */

export const feedSelector = (state) => state[moduleName].feed;

export const getSortedFeed = createSelector([feedSelector], (feed) => {
  if (feed) {

    return feed.sort((a, b) => {
      if (moment(a.date) > moment(b.date)) {
        return -1
      } else {
        return 1
      }
    })
  }
})

export const getAvailibleDates = createSelector([feedSelector], (feed) => {
  if (feed) {
    let dates = []
    for (const item of feed) {
      if (!dates.includes(item.createDate)) {
        dates.push(item.createDate)
      }
    }
    return dates
  }
})

/**
 * Custom Hooks
 */

// export const useAuthorized = () => {
//   const user = useSelector(userSelector);

//   return !!user;
// };

// export const useAdmin = () => {
//   const user = useSelector(isAdmin)

//   return !!user;
// };

/**
 * Action Creators
 * */

export const fetchFeedFromServer = () => ({
  type: FETCH_FEED_REQUEST,
});

export const deleteFromFeed = (itemId, commentId) => ({
  type: commentId ? DELETE_FEED_ITEM_COMMENT : DELETE_FEED_ITEM,
  payload: { itemId, commentId }
});

// export const addMotivation = (motivation) => ({
//   type: ADD_FEED_REQUEST,
//   payload: motivation
// });

/**
 * Sagas
 */

export const createFeedChanel = () => eventChannel(emit => apiService.fetchFeed(emit));

export const syncFeedState = function* () {
  const chanel = yield call(createFeedChanel);

  while (true) {
    const { feed } = yield take(chanel);

    if (feed) {
      yield put({
        type: FETCH_FEED_SUCCESS,
        payload: { feed },
      });
    } else {
      yield put({
        type: FETCH_FEED_ERROR,
      });
    }
  }
};

export const deleteItemSaga = function* ({ payload }) {
  yield call(apiService.removeItem, payload);
}


export const saga = function* () {
  yield all([takeEvery(FETCH_FEED_REQUEST, syncFeedState)]);
  yield all([takeEvery([DELETE_FEED_ITEM, DELETE_FEED_ITEM_COMMENT], deleteItemSaga)]);
};
