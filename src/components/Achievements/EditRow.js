import React, { useEffect, useState, useCallback } from 'react';
import Grid from '@material-ui/core/Grid';
import { Button } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import apiService from '../../services/api'


const EditRow = ({ itemData, onSave }) => {
  const [title, setTitle] = useState(itemData?.title || '')
  const [image, setImage] = useState(itemData?.image || '')
  const [fileName, setFileName] = useState('')

  useEffect(() => {
    if (image) {
      setImage(image)
    }
  }, [image])

  const onImageSave = (url) => {
    onSave({ ...itemData, image: url })
    setImage(url)
  }

  const changeHandler = useCallback(async (value, type) => {
    if (type === 'title') {
      setTitle(value)
      onSave({ ...itemData, [type]: value })
    } else {
      const file = value.files[0]
      apiService.uploadImage(file, onImageSave)
      setFileName(file.name)
    }
  }, [])

  const deleteImageHandler = () => {
    apiService.removeImage(fileName)
    setFileName('')
    onImageSave('')
  }

  return (
    <>
      <Grid xs={12}>
        <TextField
          variant="outlined"
          fullWidth
          value={title}
          onChange={(event) => changeHandler(event.target.value, 'title')}
        />
      </Grid>

      {image ? <Grid xs={12}>
        <img src={image} alt="" />
      </Grid>
        : null}

      {!image ? <Grid xs={12}>
        <Button
          variant="contained"
          component="label"
        >
          Загрузить изображение
          <input
            type="file"
            hidden
            onChange={(ev) => changeHandler(ev.target, 'image')}
          />
        </Button>
      </Grid> : null}

      {image ? <Grid xs={12}>
        <Button
          variant="contained"
          component="label"
          onClick={deleteImageHandler}
        >
          Удалить изображение
        </Button>
      </Grid> : null}

    </>
  )
}

export default EditRow