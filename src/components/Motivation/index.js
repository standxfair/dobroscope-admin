import React, { useEffect, useState, useRef } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { DataGrid } from '@material-ui/data-grid';
import Grid from '@material-ui/core/Grid';
import { Button } from '@material-ui/core';
import { useDispatch, useSelector } from "react-redux";
import { v4 as uuid } from "uuid";
import { fetchMotivationFromServer, addMotivation } from '../../ducks/motivation'
import AddItemForm from './AddItemForm'
import ConfirmDialog from '../ui/ConfirmDialog'
import CSVRead from '../common/CSVRead'
import { parseCSVMotivationList } from '../../helpers/parseCSV'
import { motivationItem } from './constants'


const columns = [
  { field: 'type', headerName: 'Тип', width: 100 },
  { field: 'title', headerName: 'Текст', width: 800 },
  { field: 'author', headerName: 'Автор', width: 200 },
  { field: 'titleEng', headerName: 'Текст Eng', width: 800 },
  { field: 'authorEng', headerName: 'Автор Eng', width: 200 },
];

export default function Motivation() {
  const [selectedItems, setSelectedItems] = useState([])
  const [confirmOpen, setConfirmOpen] = useState(false)
  const [editedItem, setEditedItem] = useState(null)
  const [CSVData, setCSVData] = useState([])
  const [CSVConfirmOpen, setCSVConfirmOpen] = useState(false)
  const dispatch = useDispatch();
  const motivationDict = useSelector(state => state.motivation?.motivationDict)
  const loading = useSelector(state => state.motivation?.loading)

  const bottomForm = useRef(null)
  const addFileInput = useRef(null)

  const addHandler = ({ title, author, type, titleEng, authorEng }) => {
    const elem = { ...motivationItem }
    elem.id = uuid()
    elem.title = title
    elem.author = author
    elem.type = type
    elem.titleEng = titleEng
    elem.authorEng = authorEng

    const newMotivation = [...motivationDict]
    newMotivation.push(elem)
    dispatch(addMotivation(newMotivation))
  }

  const deleteHandler = () => {
    let newMotivation = []
    for (let todo of motivationDict) {
      if (!selectedItems.includes(todo.id)) {
        newMotivation.push(todo)
      }
    }

    dispatch(addMotivation(newMotivation))
    setSelectedItems([])
  }

  useEffect(() => {
    if (!motivationDict) {
      dispatch(fetchMotivationFromServer())
    }
  }, [motivationDict, dispatch])

  const editHandler = ({ title, author, type, titleEng, authorEng }) => {
    let newMotivation = []
    for (let item of motivationDict) {
      if (item.id === editedItem.id) {
        item.type = type
        item.title = title
        item.author = author
        item.titleEng = titleEng
        item.authorEng = authorEng
      }
      newMotivation.push(item)
    }
    dispatch(addMotivation(newMotivation))
    setSelectedItems([])
    setEditedItem(null)
  }

  const editClick = () => {
    bottomForm.current?.scrollIntoView({ behavior: "smooth" })
    const elem = motivationDict.find((item) => item.id == selectedItems[0])
    setEditedItem(elem)
  }

  const openAddFromFileConfirm = () => {
    if (CSVData.length) {
      setCSVConfirmOpen(true)
    }
  }

  const addDataFromFile = () => {
    if (!CSVData.length) return

    const data = parseCSVMotivationList(CSVData)
    const newDict = [...motivationDict]
    data.forEach(todo => {
      const elem = { ...motivationItem }
      elem.id = uuid()
      elem.title = todo.title
      elem.author = todo.author || ''
      elem.authorEng = todo.authorEng || ''
      elem.titleEng = todo.titleEng || ''
      elem.type = todo.type || ''

      newDict.push(elem)
    })
    dispatch(addMotivation(newDict))
    setCSVData([])
    if (addFileInput.current) {
      addFileInput.current.removeFile()
    }
  }

  const onRemoveFile = () => {
    setCSVData([])
  }

  if (loading || !motivationDict) {
    return <CircularProgress />
  }

  return (
    <>
      <h1>База Мотивации</h1>
      <div style={{ height: 800, width: '100%' }}>
        <DataGrid
          autoHeight={true}
          rows={motivationDict}
          columns={columns}
          pageSize={10}
          checkboxSelection
          onSelectionChange={(e) => setSelectedItems(e.rowIds)} />
      </div>

      <Grid item xs={12} md={3}>
        {selectedItems.length === 1 ? (
          <Button variant="contained" fullWidth color="primary" onClick={() => editClick()}>
            Редактировать
          </Button>
        ) : null}

        {selectedItems.length ? <div style={{ paddingTop: 20 }}>
          <Button variant="contained" fullWidth color="secondary" onClick={() => setConfirmOpen(true)}>
            Удалить выбранные
          </Button>
          <ConfirmDialog
            open={confirmOpen}
            setOpen={setConfirmOpen}
            onConfirm={deleteHandler}
          >
            Вы уверены что хотите удалить данные без возможности восстановления?
          </ConfirmDialog>
        </div> : null}
      </Grid>

      <div style={{ paddingTop: 20 }}>
        {editedItem ? <h2>Редактировать</h2> : <h2>Добавить элемент</h2>}
        <Grid container spacing={3}>
          <Grid item xs={12} ref={bottomForm} >
            <AddItemForm
              submit={addHandler}
              editedItem={editedItem}
              editHandler={editHandler}
              setEditedItem={setEditedItem}
            />
          </Grid>
        </Grid>
      </div>

      <Grid container spacing={3} style={{ paddingTop: 20 }}>
        <Grid item xs={12}>
          <h2>Загрузить данные из CSV-файла</h2>
        </Grid>
        <Grid item xs={12} md={3}>
          <CSVRead onFileAdd={setCSVData} reference={addFileInput} onRemoveFile={onRemoveFile} />
          <Button
            variant="contained"
            color="primary"
            onClick={openAddFromFileConfirm}
            style={{ marginTop: 20 }}
            fullWidth
          >
            {'Добавить'}
          </Button>
          {CSVData.length ? <ConfirmDialog
            open={CSVConfirmOpen}
            setOpen={setCSVConfirmOpen}
            onConfirm={addDataFromFile}
            cancelButtonText='Отмена'
            submitButtonText='OK'
          >
            Добавить данные из файла?
          </ConfirmDialog> : null}
        </Grid>
      </Grid>
    </>
  );
}
