import app from 'firebase/app';
import "firebase/auth";
import 'firebase/database';
import 'firebase/storage'
import { firebaseConfig } from "../config";

class ApiService {
  constructor(firebaseConfig) {
    this.fb = app.initializeApp(firebaseConfig);
    this.database = app.database();
    this.storage = app.storage()
  }

  signUp = (email, password) => this.fb.auth().createUserWithEmailAndPassword(email, password);

  signIn = (email, password) => this.fb.auth().signInWithEmailAndPassword(email, password);

  signOut = () => this.fb.auth().signOut();

  onAuthChange = (callback) => this.fb.auth().onAuthStateChanged(callback);

  addAdmin = (userId) => this.database.ref('admins/' + userId).set({
    userId
  }).then(() => true)

  fetchAdmins = () => this.database
    .ref('/admins/')
    .once('value').then((snapshot) => snapshot.val())

  fetchTodos = (callback) => this.database
    .ref('todosDict/')
    .on('value', (snapshot) => callback({ todosDict: snapshot.val() }));

  fetchMotivation = (callback) => this.database
    .ref('motivationDict/')
    .on('value', (snapshot) => callback({ motivationDict: snapshot.val() }));

  fetchFeed = (callback) => this.database
    .ref('feed/')
    .on('value', (snapshot) => {
      if (snapshot.val()) {
        const feed = processFbCollection(snapshot.val())
        for (let item of feed) {
          if (item.comments) {
            item.comments = [...processFbCollection(item.comments)]
          }
        }
        return callback({ feed })
      } else {
        return callback({ feed: null })
      }
    });

  fetchFeedback = (callback) => this.database
    .ref('feedback/')
    .on('value', (snapshot) => callback({ feedback: snapshot.val() ? processFbCollection(snapshot.val()) : null }));

  checkMessage = (data) => this.database.ref('feedback/' + data.id).update({ isNew: data.value })

  updateTodosDict = (data) => this.database.ref('todosDict/').set(data)

  updateMotivationDict = (data) => this.database.ref('motivationDict/').set(data)

  removeItem = (data) => {
    if (!data.commentId) {
      this.database
        .ref('feed/')
        .child(data.itemId)
        .remove()
    } else { //delete comment
      this.database
        .ref('feed/' + data.itemId + '/comments/')
        .child(data.commentId)
        .remove()
    }
  }

  removeMessageItem = (data) => {
    this.database
      .ref('feedback/')
      .child(data.id)
      .remove()
  }

  fetchClients = (callback) => this.database
    .ref('users/')
    .on('value', (snapshot) => callback({ clients: snapshot.val() ? processFbCollection(snapshot.val()) : null }));

  deleteClient = (data) => this.database
    .ref('users/')
    .child(data.id)
    .remove()

  saveClient = (data) => this.database
    .ref('users/' + data.id)
    .set({ ...removeField('id', data) })

  fetchTexts = (callback) => this.database
    .ref('texts/')
    .on('value', (snapshot) => callback({ texts: snapshot.val() ? (snapshot.val()) : null }));

  addTexts = (texts) => {
    this.database
      .ref('texts/')
      .set(texts)
      .then(() => true)
  }

  fetchAchievs = (callback) => this.database
    .ref('achievements/')
    .on('value', (snapshot) => callback({ achievements: snapshot.val() ? processFbCollection(snapshot.val()) : null }));

  addAchievs = (achievements) => {
    this.database
      .ref('achievements/')
      .set(achievements)
      .then(() => true)
  }

  uploadImage = (imageFile, callback) => this.storage
    .ref(`/images/${imageFile.name}`)
    .put(imageFile)
    .on('state_changed',
      (snapshot) => { },
      (e) => { console.log(e) },
      () => {
        this.storage.ref('images')
          .child(imageFile.name)
          .getDownloadURL()
          .then(fireBaseUrl => {
            callback(fireBaseUrl)
          })
      }
    )

  removeImage = (fileName) => this.storage
    .ref(`/images/${fileName}`)
    .delete()
    .then(() => true)
}

export const processFbCollection = (collection) => Object.keys(collection).map((id) => ({
  id,
  ...collection[id]
}))

export const removeField = (fieldName, collection) => {
  let result = {}
  Object.keys(collection).forEach(key => {
    if (key !== fieldName) {
      result[key] = collection[key]
    }
  })
  return result
}

export default new ApiService(firebaseConfig);
