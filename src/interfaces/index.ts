export interface IState {
  users?: IStateUsers
}

interface IStateUsers {
  clients?: object[]
}

export interface IFeedItem {
  avatar: string
  author: string
  createDate: string
  text: string
}

export interface IMessagesItem {
  userName: string
  createDate: string
  message: string
}