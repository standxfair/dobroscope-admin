import { createStore, applyMiddleware } from "redux";
// import { routerMiddleware } from "connected-react-router";
// import logger from "redux-logger";
import createSagaMiddleware from "redux-saga";
import { composeWithDevTools } from 'redux-devtools-extension'
import reducer from "./reducer";
// import history from "../history";
import rootSaga from "./saga";

export const initStore = () => {
  const sagaMiddleware = createSagaMiddleware();

  let enhancer
  if (process.env.NODE_ENV !== 'production') {
    enhancer = composeWithDevTools(applyMiddleware(
      sagaMiddleware,
      // routerMiddleware(history),
    ));
  } else {
    enhancer = applyMiddleware(
      sagaMiddleware,
      // routerMiddleware(history),
    );
  }

  const store = createStore(reducer, enhancer);

  sagaMiddleware.run(rootSaga);

  return store;
};
