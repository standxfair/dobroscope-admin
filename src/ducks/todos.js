import { all, call, put, take, takeEvery, } from "redux-saga/effects";
import { eventChannel } from "redux-saga";
import { appName } from "../config";
import { Record } from "immutable";
import apiService from "../services/api";


/**
 * Constants
 * */
export const moduleName = "todos";
const prefix = `${appName}/${moduleName}`;


export const FETCH_TODOS_REQUEST = `${prefix}/FETCH_TODOS_REQUEST`;
export const FETCH_TODOS_SUCCESS = `${prefix}/FETCH_TODOS_SUCCESS`;
export const FETCH_TODOS_ERROR = `${prefix}/FETCH_TODOS_ERROR`;

export const ADD_TODOS_REQUEST = `${prefix}/ADD_TODOS_REQUEST`;
export const ADD_TODOS_SUCCESS = `${prefix}/ADD_TODOS_SUCCESS`;

/**
 * Reducer
 * */
export const ReducerRecord = Record({
  todosDict: null,
  loading: false,
  error: null,
});

export default function reducer(state = new ReducerRecord(), action) {
  const { type, payload, error } = action;

  switch (type) {
    case FETCH_TODOS_REQUEST:
      return state.set("loading", true);

    case FETCH_TODOS_SUCCESS:
      return state
        .set("loading", false)
        .set("todosDict", payload.todosDict)
        .set("error", null);

    case ADD_TODOS_SUCCESS:
      return state
        .set("loading", false)
        .set("todosDict", payload.todosUpdate)
        .set("error", null);

    case FETCH_TODOS_ERROR:
      return state.set("loading", false);

    default:
      return state;
  }
}

/**
 * Selectors
 * */

// export const userSelector = (state) => state[moduleName].user;

// const getAdmins = state => state[moduleName]?.admins
// export const isAdmin = createSelector([getAdmins, userSelector], (admins, user) => {
//   if (!!admins && !!user) {
//     return !!admins[user.uid]
//   }
// })

/**
 * Custom Hooks
 */

// export const useAuthorized = () => {
//   const user = useSelector(userSelector);

//   return !!user;
// };

// export const useAdmin = () => {
//   const user = useSelector(isAdmin)

//   return !!user;
// };

/**
 * Action Creators
 * */

export const fetchTodosFromServer = () => ({
  type: FETCH_TODOS_REQUEST,
});

export const addTodos = (todos) => ({
  type: ADD_TODOS_REQUEST,
  payload: todos
});

/**
 * Sagas
 */

export const createTodosChanel = () => eventChannel(emit => apiService.fetchTodos(emit));

export const syncTodosState = function* () {
  const chanel = yield call(createTodosChanel);

  while (true) {
    const { todosDict } = yield take(chanel);

    if (todosDict) {
      yield put({
        type: FETCH_TODOS_SUCCESS,
        payload: { todosDict },
      });
    } else {
      yield put({
        type: FETCH_TODOS_ERROR,
      });
    }
  }
};

export const addTodosSaga = function* ({ payload }) {
  yield call(apiService.updateTodosDict, payload);
}


export const saga = function* () {
  yield all([takeEvery(FETCH_TODOS_REQUEST, syncTodosState)]);
  yield all([takeEvery(ADD_TODOS_REQUEST, addTodosSaga)]);
};
