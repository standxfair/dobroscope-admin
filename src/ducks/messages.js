import { all, call, put, take, takeEvery, } from "redux-saga/effects";
import { eventChannel } from "redux-saga";
import { createSelector } from "reselect"
import { Record } from "immutable";
import moment from 'moment'
import { appName } from "../config";
import apiService from "../services/api";


/**
 * Constants
 * */
export const moduleName = "messages";
const prefix = `${appName}/${moduleName}`;


export const FETCH_MESSAGES_REQUEST = `${prefix}/FETCH_MESSAGES_REQUEST`;
export const FETCH_MESSAGES_SUCCESS = `${prefix}/FETCH_MESSAGES_SUCCESS`;
export const FETCH_MESSAGES_ERROR = `${prefix}/FETCH_MESSAGES_ERROR`;

export const ADD_MESSAGES_REQUEST = `${prefix}/ADD_MESSAGES_REQUEST`;

export const DELETE_MESSAGES_ITEM = `${prefix}/DELETE_MESSAGES_ITEM`;

export const SET_MESSAGE_STATUS = `${prefix}/SET_MESSAGE_STATUS`;


/**
 * Reducer
 * */
export const ReducerRecord = Record({
  feedback: null,
  loading: false,
  error: null,
});

export default function reducer(state = new ReducerRecord(), action) {
  const { type, payload, error } = action;

  switch (type) {
    case FETCH_MESSAGES_REQUEST:
      return state.set("loading", true);

    case FETCH_MESSAGES_SUCCESS:
      return state
        .set("loading", false)
        .set("feedback", payload.feedback)
        .set("error", null);

    default:
      return state;
  }
}

/**
 * Selectors
 * */

export const feedbackSelector = (state) => state[moduleName].feedback;

export const getSortedFeedback = createSelector([feedbackSelector], (feedback) => {
  if (feedback) {
    return feedback.sort((a, b) => {
      if (moment(a.date) > moment(b.date)) {
        return -1
      } else {
        return 1
      }
    })
  }
})

export const getSortedNewMessages = createSelector([getSortedFeedback], (feedback) => {
  if (feedback) {
    return feedback.filter(item => item.isNew)
  }
})


/**
 * Custom Hooks
 */

// export const useAuthorized = () => {
//   const user = useSelector(userSelector);

//   return !!user;
// };

// export const useAdmin = () => {
//   const user = useSelector(isAdmin)

//   return !!user;
// };

/**
 * Action Creators
 * */

export const fetchFeedbackFromServer = () => ({
  type: FETCH_MESSAGES_REQUEST,
});

export const deleteFromFeedback = (id) => ({
  type: DELETE_MESSAGES_ITEM,
  payload: { id }
});

export const setMessageStatus = (id, value) => ({
  type: SET_MESSAGE_STATUS,
  payload: { id, value }
});



/**
 * Sagas
 */

export const createFeedbackChanel = () => eventChannel(emit => apiService.fetchFeedback(emit));

export const syncFeedbackState = function* () {
  const chanel = yield call(createFeedbackChanel);

  while (true) {
    const { feedback } = yield take(chanel);

    if (feedback) {
      yield put({
        type: FETCH_MESSAGES_SUCCESS,
        payload: { feedback },
      });
    } else {
      yield put({
        type: FETCH_MESSAGES_ERROR,
      });
    }
  }
};

export const setMessageStatusSaga = function* ({ payload }) {
  yield call(apiService.checkMessage, payload);
}

export const deleteFeedbackItemSaga = function* ({ payload }) {
  yield call(apiService.removeMessageItem, payload);
}


export const saga = function* () {
  yield all([
    takeEvery(FETCH_MESSAGES_REQUEST, syncFeedbackState),
    takeEvery(SET_MESSAGE_STATUS, setMessageStatusSaga),
    takeEvery(DELETE_MESSAGES_ITEM, deleteFeedbackItemSaga),
  ]);
};
