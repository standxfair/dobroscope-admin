import { all, call, put, take, delay, takeEvery } from "redux-saga/effects";
import { eventChannel } from "redux-saga";
import { createSelector } from "reselect"
import { appName } from "../config";
import { Record } from "immutable";
import apiService from "../services/api";
import { useSelector } from "react-redux";

/**
 * Constants
 * */
export const moduleName = "auth";
const prefix = `${appName}/${moduleName}`;

export const SIGN_UP_REQUEST = `${prefix}/SIGN_UP_REQUEST`;
export const SIGN_UP_START = `${prefix}/SIGN_UP_START`;
export const SIGN_UP_SUCCESS = `${prefix}/SIGN_UP_SUCCESS`;
export const SIGN_UP_ERROR = `${prefix}/SIGN_UP_ERROR`;
export const SIGN_UP_TIMEOUT_LIMIT = `${prefix}/SIGN_UP_TIMEOUT_LIMIT`;
export const SIGN_UP_HARD_LIMIT = `${prefix}/SIGN_UP_HARD_LIMIT`;

export const SIGN_OUT_SUCCESS = `${prefix}/SIGN_OUT_SUCCESS`;
export const SIGN_IN_REQUEST = `${prefix}/SIGN_IN_REQUEST`;
export const SIGN_IN_SUCCESS = `${prefix}/SIGN_IN_SUCCESS`;
export const SIGN_IN_ERROR = `${prefix}/SIGN_IN_ERROR`;
export const CLEAR_LOGIN_FORM = `${prefix}/CLEAR_LOGIN_FORM`;



export const FETCH_ADMINS_REQUEST = `${prefix}/FETCH_ADMINS_REQUEST`;
export const FETCH_ADMINS_SUCCESS = `${prefix}/FETCH_ADMINS_SUCCESS`;

export const ADD_ADMIN_REQUEST = `${prefix}/ADD_ADMIN_REQUEST`;
export const ADD_ADMIN_SUCCESS = `${prefix}/ADD_ADMIN_SUCCESS`;
export const SET_IS_NEW_USER = `${prefix}/SET_IS_NEW_USER`;

/**
 * Reducer
 * */
export const ReducerRecord = Record({
  user: null,
  loading: false,
  error: null,
  admins: null,
  isNewUser: false,
});

export default function reducer(state = new ReducerRecord(), action) {
  const { type, payload, error } = action;

  switch (type) {
    case SIGN_UP_START:
      return state.set("loading", true);

    case SIGN_IN_SUCCESS:
      return state
        .set("loading", false)
        .set("user", payload.user)
        .set("error", null);

    case SIGN_OUT_SUCCESS:
      return state
        .set("user", null)
        .set("loading", false)

    case SIGN_UP_ERROR:
    case SIGN_IN_ERROR:
      return state
        .set("error", error)
        .set("loading", false);

    case CLEAR_LOGIN_FORM:
      return state
        .set("error", null)
        .set("loading", false)
        .set("user", null)

    case FETCH_ADMINS_SUCCESS:
      return state.set("admins", payload.admins)

    case SIGN_UP_SUCCESS:
    case SET_IS_NEW_USER:
      return state.set("isNewUser", payload.isNewUser)

    default:
      return state;
  }
}

/**
 * Selectors
 * */

export const userSelector = (state) => state[moduleName].user;
export const userIdSelector = (state) => state[moduleName].user?.uid;

const getAdmins = state => state[moduleName]?.admins
export const isAdmin = createSelector([getAdmins, userSelector], (admins, user) => {
  if (!!admins && !!user) {
    return !!admins[user.uid]
  }
})

/**
 * Custom Hooks
 */

export const useAuthorized = () => {
  const user = useSelector(userSelector);

  return !!user;
};

export const useAdmin = () => {
  const user = useSelector(isAdmin)

  return !!user;
};



/**
 * Action Creators
 * */

export const signUp = (email, password) => ({
  type: SIGN_UP_REQUEST,
  payload: { email, password },
});

export const signIn = (email, password) => ({
  type: SIGN_IN_REQUEST,
  payload: { email, password },
});

export const fetchAdmins = () => ({
  type: FETCH_ADMINS_REQUEST,
});

export const addUserToAdmins = (userId) => ({
  type: ADD_ADMIN_REQUEST,
  payload: { userId },
});

export const clearForm = () => ({
  type: CLEAR_LOGIN_FORM,
});

/**
 * Sagas
 */
export const fetchAdminsSaga = function* ({ payload }) {
  const { user } = payload

  const admins = yield call(apiService.fetchAdmins);

  yield put({
    type: FETCH_ADMINS_SUCCESS,
    payload: { admins },
  });

  if (!admins[user.uid]) {
    yield put({
      type: SET_IS_NEW_USER,
      payload: { isNewUser: true },
    });
  } else {
    yield put({
      type: SET_IS_NEW_USER,
      payload: { isNewUser: false },
    });
  }
}


export const signInSaga = function* () {
  let errorCount = 0;

  while (true) {
    if (errorCount === 3) {
      yield put({
        type: SIGN_UP_TIMEOUT_LIMIT,
      });

      yield delay(1000);
    } else if (errorCount >= 5) {
      yield put({
        type: SIGN_UP_HARD_LIMIT,
      });

      return;
    }

    const signInData = yield take(SIGN_IN_REQUEST);

    if (signInData) {
      const { payload: { email, password } } = signInData

      try {
        const user = yield call(apiService.signIn, email, password);

        yield put({
          type: SIGN_IN_SUCCESS,
          payload: { user },
        });
      } catch (error) {
        yield put({
          type: SIGN_IN_ERROR,
          error,
        });
      }
    }

    const {
      payload: { email, password },
    } = yield take(SIGN_UP_REQUEST);

    yield put({
      type: SIGN_UP_START,
    });

    try {
      const user = yield call(apiService.signUp, email, password);

      yield put({
        type: SIGN_UP_SUCCESS,
        payload: { user },
      });
    } catch (error) {
      errorCount++;

      yield put({
        type: SIGN_UP_ERROR,
        error,
      });
    }
  }
};

export const signUpSaga = function* () {
  while (true) {
    const signUpData = yield take(SIGN_UP_REQUEST);

    if (signUpData) {
      const { payload: { email, password } } = signUpData

      try {
        yield call(apiService.signUp, email, password);

        yield put({
          type: SIGN_UP_SUCCESS,
          payload: { isNewUser: true },
        });
      } catch (error) {
        yield put({
          type: SIGN_UP_ERROR,
          error,
        });
      }
    }
  }
}

export const createAuthChanel = () =>
  eventChannel((emit) => apiService.onAuthChange((user) => emit({ user })));

export const syncAuthState = function* () {
  yield put({
    type: SIGN_UP_START,
  });
  const chanel = yield call(createAuthChanel);

  while (true) {
    const { user } = yield take(chanel);

    if (user) {
      yield put({
        type: SIGN_IN_SUCCESS,
        payload: { user },
      });
    } else {
      yield put({
        type: SIGN_OUT_SUCCESS,
      });
    }
  }
};

export const adminSaga = function* ({ payload, type }) {
  if (type === ADD_ADMIN_REQUEST) {
    const addAdmin = yield call(apiService.addAdmin, payload.userId)
    console.log('addAdmin', addAdmin);
  }

}

export const saga = function* () {
  yield all([
    signInSaga(),
    signUpSaga(),
    syncAuthState(),
    takeEvery(SIGN_IN_SUCCESS, fetchAdminsSaga),
    takeEvery(ADD_ADMIN_REQUEST, adminSaga)
  ]);

};
