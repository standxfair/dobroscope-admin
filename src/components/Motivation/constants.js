export const motivationItem = {
  id: null,
  type: '',
  title: '',
  author: '',
  titleEng: '',
  authorEng: ''
}

export const types = ['Цитата', 'Мотивация']