import { spawn } from "redux-saga/effects";
import { saga as authSaga } from "../ducks/auth";
import { saga as todosSaga } from "../ducks/todos";
import { saga as motivationSaga } from "../ducks/motivation";
import { saga as feedSaga } from "../ducks/feed";
import { saga as messagesSaga } from "../ducks/messages";
import { saga as usersSaga } from "../ducks/users";
import { saga as textsSaga } from "../ducks/texts";
import { saga as achievsSaga } from "../ducks/achievements";

export default function* () {
  yield spawn(authSaga);
  yield spawn(todosSaga);
  yield spawn(motivationSaga);
  yield spawn(feedSaga);
  yield spawn(messagesSaga);
  yield spawn(usersSaga);
  yield spawn(textsSaga);
  yield spawn(achievsSaga);
}
