import React from 'react';
import { useDispatch } from "react-redux";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Button } from '@material-ui/core';
import Link from '@material-ui/core/Link';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { addUserToAdmins } from '../../ducks/auth'
import apiService from '../../services/api'

const validationSchema = yup.object({
  uid: yup
    .string('Введите id')
    .required('Поле обязательно'),
});

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    overflow: 'hidden',
    padding: theme.spacing(0, 3),
  },
  paper: {
    // maxWidth: 400,
    margin: `${theme.spacing(1)}px auto`,
    padding: theme.spacing(2),
  },
  itemHeader: {
    // background: '#eee',
    alignItems: 'center',
    marginBottom: theme.spacing(2),
  },
  date: {
    fontSize: 12,
    marginBottom: -20,
    color: '#837070'
  },
  showComment: {
    cursor: 'pointer'
  },
  img: {
    objectFit: 'cover',
    width: '100%',
    maxHeight: 200,
  },
  commentsCont: {
    marginTop: -30,
  }

}));

export default function Settings() {
  const formik = useFormik({
    initialValues: {
      uid: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values, e) => {
      const { uid } = values

      dispatch(addUserToAdmins(uid))
      e.resetForm({})
    },
  });

  const dispatch = useDispatch();
  const classes = useStyles();

  return (
    <>
      <h1>Настройки</h1>
      <Grid container spacing={3} className={classes.root}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <h3>Добавить администратора</h3>
            <form onSubmit={formik.handleSubmit}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="uid"
                label="uid"
                name="uid"
                autoComplete="off"
                value={formik.values.uid}
                onChange={formik.handleChange}
                error={formik.touched.uid || Boolean(formik.errors.uid)}
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                style={{ marginTop: 20 }}
              >
                Добавить
               </Button>
            </form>
          </Paper>
        </Grid>

      </Grid>

      <Grid container spacing={3} className={classes.root}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Link
              component="button"
              variant="body2"
              onClick={() => apiService.signOut()}
            >
              Выйти из аккаунта
          </Link>
          </Paper>
        </Grid>
      </Grid>

    </>
  );
}
