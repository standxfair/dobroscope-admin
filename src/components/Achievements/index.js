import React, { useEffect, useState, useCallback } from 'react';
import Grid from '@material-ui/core/Grid';
import { useDispatch, useSelector } from "react-redux";
import { fetchAchievsFromServer, addAchievements } from '../../ducks/achievements'
import { DataGrid } from '@material-ui/data-grid';
import ConfirmDialog from '../ui/ConfirmDialog'
import EditRow from './EditRow'

const columns = [
  { field: 'title', headerName: 'Название', width: 500 },
  { field: 'image', headerName: 'Изображение', width: 300 }
];

const Achievements = () => {
  const [editId, setEditId] = useState(null)
  const [rowData, setRowData] = useState({})
  const dispatch = useDispatch();
  const achievements = useSelector(state => state.achievements?.achievements)

  useEffect(() => {
    dispatch(fetchAchievsFromServer())
  }, [])

  const editRowHandler = useCallback(({ rowIds }) => {
    const id = rowIds[0]
    setEditId(id)
  }, [])

  const onConfirm = useCallback(() => {
    const res = [...achievements]
    res.forEach((el) => {
      if (el?.id === editId) {
        el.title = rowData.title || ''
        el.image = rowData.image || ''
      }
    })

    dispatch(addAchievements(res))
    setEditId(null)
  }, [achievements, editId, rowData, dispatch])

  const onCancel = useCallback(() => {
    setRowData(null)
  }, [])


  if (!achievements) return null

  return <>
    <h1>Награды</h1>
    <Grid item xs={12}>
      <div style={{ height: 500, width: '100%' }}>
        <DataGrid
          autoHeight={true}
          rows={achievements}
          columns={columns}
          pageSize={10}
          onSelectionChange={editRowHandler}
        />
      </div>
    </Grid>
    <ConfirmDialog
      open={editId}
      setOpen={setEditId}
      submitButtonText="Сохранить"
      fullWidth={true}
      onConfirm={onConfirm}
      onCancel={onCancel}
    >
      <EditRow
        itemData={achievements.find((el) => el.id === editId)}
        onSave={setRowData}
      />
    </ConfirmDialog>
  </>
}

export default Achievements