import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { FormHelperText } from '@material-ui/core';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useDispatch, useSelector } from "react-redux";
import { signUp, useAuthorized, useAdmin, signIn, clearForm } from '../../ducks/auth'
import BackLoader from '../ui/BackLoader'


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
        Dobroscope App{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const validationSchema = yup.object({
  email: yup
    .string('Введите email')
    .email('Введите email')
    .required('Поле обязательно'),
  password: yup
    .string('Введите пароль')
    .min(8, 'Минимальная длина пароля 8 символов')
    .required('Поле обязательно'),
});

export default function LoginForm() {
  const classes = useStyles();
  const admin = useAdmin()
  const dispatch = useDispatch();
  const isUser = useAuthorized()
  const loading = useSelector(state => state.auth.loading)
  const error = useSelector(state => state.auth.error)
  const isNewUser = useSelector(state => state.auth.isNewUser)

  const [formState, setFormState] = useState('login')

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values, e) => {
      const { email, password } = values

      if (formState === 'login') {
        dispatch(signIn(email, password))
      } else {
        dispatch(signUp(email, password))
      }
    },
  });

  if (loading || (isUser && !admin)) {
    return (
      <BackLoader
        open={loading || (isUser && !admin)}
        isNewUser={isNewUser}
      />
    )
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          {formState === 'login' ? 'Вход' : 'Регистрация'}
        </Typography>
        <form className={classes.form} onSubmit={formik.handleSubmit}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email"
            name="email"
            autoComplete="off"
            autoFocus
            value={formik.values.email}
            onChange={formik.handleChange}
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email ? formik.errors.email : ''}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Пароль"
            type="password"
            id="password"
            autoComplete="current-password"
            value={formik.values.password}
            onChange={formik.handleChange}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.password ? formik.errors.password : ''}
          />

          {error ? <FormHelperText>{error.message}</FormHelperText> : null}

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            {formState === 'login' ? 'Войти' : 'Зарегистрироваться'}
          </Button>
          <Button
            fullWidth
            color="secondary"
            className={classes.submit}
            onClick={(e, val) => {
              dispatch(clearForm())
              formik.resetForm({})
              if (formState === 'login') {
                setFormState('registration')
              } else {
                setFormState('login')
              }
            }}
          >
            {formState === 'login' ? 'Зарегистрироваться' : 'Назад ко входу'}
          </Button>

        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}