import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Paper from '@material-ui/core/Paper';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Modal from '@material-ui/core/Modal';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import ReplyIcon from '@material-ui/icons/Reply';
import Tooltip from '@material-ui/core/Tooltip';
import useMediaQuery from '@material-ui/core/useMediaQuery';


import { fetchFeedbackFromServer, getSortedFeedback, getSortedNewMessages, setMessageStatus, deleteFromFeedback } from '../../ducks/messages'

function getModalStyle() {
  const top = 50
  const left = 50
  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 800,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function Messages() {
  const dispatch = useDispatch();
  const feedback = useSelector(getSortedFeedback)
  const newMessages = useSelector(getSortedNewMessages)
  const [tabValue, setTabValue] = useState(0);
  const [currentOpen, setCurrentOpen] = useState(null);

  useEffect(() => {
    if (!feedback) {
      dispatch(fetchFeedbackFromServer())
    }
  }, [feedback, dispatch])

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
  };

  const handleClose = () => {
    dispatch(setMessageStatus(currentOpen, false))
    setCurrentOpen(false);
  };

  if (!feedback) {
    return <div>Нет сообщений</div>
  }

  return (
    <>
      <Tabs
        value={tabValue}
        onChange={handleChange}
        indicatorColor="primary"
        textColor="primary"
        centered
      >
        <Tab label="Новые сообщения" />
        <Tab label="Все собщения" />
      </Tabs>
      {tabValue === 0 ? (
        newMessages?.length ? (
          <List>
            {newMessages.map((item, key) => (
              <Paper key={key} onClick={() => setCurrentOpen(item.id)}>
                <ListItem>
                  <ListItemAvatar>
                    <Avatar
                    >
                      {item.userName[0]}
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    primary={item.userName + ' | ' + item.createDate}
                    secondary={item.message.slice(0, 200) + '...'}
                  />
                </ListItem>
              </Paper>
            ))}
          </List>
        ) : (
            <List>
              <ListItem >
                <ListItemText primary={'Нет новых сообщений'} />
              </ListItem>
            </List>
          )
      ) : (
          feedback?.length ? (
            <List>
              {feedback.map((item, key) => (
                <Paper key={key} onClick={() => setCurrentOpen(item.id)}>
                  <ListItem >
                    <ListItemAvatar>
                      <Avatar
                      >
                        {item.userName[0]}
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                      primary={item.userName + ' | ' + item.createDate}
                      secondary={item.message.slice(0, 200) + '...'}
                    />
                  </ListItem>
                </Paper>
              ))}
            </List>
          ) : (
              <List>
                <ListItem >
                  <ListItemText primary={'Нет сообщений'} />
                </ListItem>
              </List>
            )
        )}

      <Modal
        open={currentOpen}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Message id={currentOpen} messages={feedback} />
      </Modal>
    </>
  );
}

const Message = ({ id, messages }) => {
  const data = messages.find(item => item.id === id)
  const classes = useStyles();
  const [modalStyle] = React.useState(getModalStyle);
  const dispatch = useDispatch();
  const matches = useMediaQuery('(max-width:375px)');
  return (
    <Card className={classes.paper} style={matches ? {} : modalStyle}>
      <CardHeader
        avatar={
          <Avatar aria-label="recipe">
            {data.userName[0]}
          </Avatar>
        }

        title={data.userName + ' | ' + data.createDate}
        subheader={data.email}
      />
      <CardContent>
        {data.message}
      </CardContent>

      <CardActions disableSpacing>
        <Tooltip title="Ответить" placement="top">
          <a href={'mailto:' + data.email}>
            <IconButton aria-label="Ответить">
              <ReplyIcon />
            </IconButton>
          </a>
        </Tooltip>
        <Tooltip title="Удалить" placement="top">
          <IconButton aria-label="Удалить" onClick={() => dispatch(deleteFromFeedback(data.id))}>
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      </CardActions>
    </Card>
  )
}